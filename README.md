# Survive The Maze
### This game is built on top of my Base Game Template.
This project uses git LFS as well as git subrepositories. To work in this repo, use the following commands:

    git lfs install
    git clone https://gitlab.com/sdggames/godot-common-library.git FOLDER
    cd FOLDER
    git submodule update --init
<br/><br/>

## Tools I made
### Automatic CICD with automatic deployment to itch.io and version tracking.
- All commits to development branches will build for common platforms and generate a HTML5 playable game in-browser, along with downloadable artifacts.
- Commits to the release branch will build a release version and publish it to itch.io
- Release builds can also be triggered and sent to a separate branch on itch.io
- TODO: unit tests will also automatically run on dev branches and master.

### Mod Pack Support
- Autoload mods have a common entry point. There is a file flag that enables a mod to load itself as a singleton by default
- Project can be configured to load mod files (including custom scripts) wherever neccessary.
- See the readme in the game/Mods folder for more usage details.

### FPS Menu
- A FPS display can be activated via menu or the command terminal plugin.
- Fps display will show the min, max, and average frame counts
<br/><br/>

## Tools other people made
### Gif Tool
- TODO: Add a game object that allows the user to create a gif in-game to share with friends
- TODO: Also allow for screenshot generation

### Command Terminal
- [Console by quentincaffeino](https://github.com/quentincaffeino/godot-console) is included in the addons folder.

### Unit Tests
- [GUT by bitwes](https://github.com/bitwes/Gut) included in the addons folder.
- TODO: Unit tests are automatically run via Gitlab

### Print Singleton
- [Logging script by spooner](https://gist.github.com/Spooner/0daff3fd31411488fe1b) included in the addons folder.
<br/><br/>

## Tools I plan on implementing in the future
### Unified Save and Load System
- TODO: Add a universal system that allows data to be easily saved to an loaded from a specific file

### Easy Creation of Developer Guis (IMGui)
- TODO: Find a quick and easy way to create developer menus from scripts. Menu tabs should be easy to add from anywhere in the project, and should not require any menu design work
  - Maybe create a menu tree on the first call and navigate it on subsequent calls? 
<br/><br/>

## Credits/Licenses/Acknowledgments
CICD solution was inspired by [barichello's example](https://gitlab.com/barichello/godot-ci)

The Mod Loader was inspired by [Ryan Forrester's example](https://ryanforrester.ca/godot/2019/08/01/creating-a-moddable-godot-game/)
