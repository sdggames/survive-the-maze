extends Spatial

export var is_player: bool = false
var maze = []
var mortals = []
var mortal_index = 0


func init(maze_nodes):
	# Save our own reference for shadows and AI to look at.
	maze = maze_nodes
	# TODO: Unlink this. We are assuming that the player is the minotaur for the AI jam.
	$PathEngine.can_traverse_unlit = is_player
	$PathEngine.create_traversable_grid(maze_nodes)
	
	# Store references to only the mortals, not the team components.
	for mortal in get_children():
		if mortal is Mortal:
			mortals.append(mortal)
			mortal.path_engine = $PathEngine
				
	# TODO: Make the AI less dumb. Probably make some things way more accessible so we don't have to pass the maze around.
	$TeamAi.init(maze_nodes, $PathEngine)
	
	if is_player:
		GlobalSettings.input_manager.connect_team(self)


func select_mortal(pos: Vector2):
	var mouse_pos = $PathEngine.mouse_to_world(pos)
	var smallest_distance = INF
	var closest_mortal
	for mortal in mortals:
		if is_instance_valid(mortal):
			var mortal_pos: Vector3 = mortal.transform.origin
			var distance = mortal_pos.distance_to(mouse_pos)
			if distance < smallest_distance:
				smallest_distance = distance
				closest_mortal = mortal
			
	if smallest_distance < GlobalSettings.selection_radius:
		for mortal in mortals:
			if is_instance_valid(mortal):
				mortal.deselect()
		closest_mortal.select()


func select_mortals_in_box():
	# The input manager has a selection box prepared, we just need to check if each mortal is inside of it.
	var selected_mortals = []
	var unselected_mortals = []
	for mortal in mortals:
		if is_instance_valid(mortal):
			if GlobalSettings.input_manager.is_mortal_in_box(mortal):
				selected_mortals.append(mortal)
			else:
				unselected_mortals.append(mortal)
	if selected_mortals.size() > 0:
		for mortal in selected_mortals:
			mortal.select()
		for mortal in unselected_mortals:
			mortal.deselect()


func move_mortal(pos: Vector2):
	var path_end = $PathEngine.mouse_to_world(pos)
	for mortal in mortals:
		if is_instance_valid(mortal) and mortal.selected:
			mortal.set_path(path_end)
			if GlobalSettings.debug_draw_navigation:
				$PathEngine.draw_path(mortal.path, mortal, false)


# TODO: Tempoarary fix for jam for team minotaur. Set up a better refresh timer for any mortal that can walk through walls!
var tick = 0

# TODO: Move this elsewhere, the AI shouldn't need this much coaching.
func _process(_delta):
	$PathEngine.update_shadows(maze)
	tick = tick + 1
	if tick > 5:
		tick = 0
		if !is_player:
			# Cycle through the mortals so we don't hit the PathEngine too hard.
			mortal_index = mortal_index + 1
			if mortal_index >= mortals.size():
				mortal_index = 0
			
			if is_instance_valid(mortals[mortal_index]):
				$TeamAi.update_mortal_path(mortals[mortal_index], mortal_index)
				if GlobalSettings.debug_draw_navigation:
					$PathEngine.draw_path(mortals[mortal_index].path, mortal_index, true)
			elif GlobalSettings.debug_draw_navigation:
				$PathEngine.clear_debug_draw(mortal_index)
