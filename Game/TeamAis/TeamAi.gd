class_name TeamAi
extends Node

var maze
var maze_size: Vector2
var path_engine: PathEngine

# TODO: This AI totally cheats right now! Make it a little more clever, and a little less unfair.
var minotaur: Mortal
var minotaur_seen = false
var game_starting = true


func init(maze_nodes, engine):
	maze = maze_nodes
	maze_size = Vector2(maze.size(), maze[0].size())
	path_engine = engine
	minotaur = find_node_by_name(get_tree().get_root(), "Minotaur")


func random_valid_vector3() -> Vector3:
	return Vector3(rand_range(-maze_size.x / 2, maze_size.x / 2), 1, rand_range(-maze_size.y / 2, maze_size.y / 2))


func update_mortal_path(mortal: Mortal, index: int):
	if is_instance_valid(mortal):
		if is_instance_valid(minotaur) and minotaur_seen:
			minotaur_seen = false # Use this once
			if mortal.global_transform.origin.distance_to(minotaur.global_transform.origin) < 7:
				path_near_minotaur(mortal)
			else:
				 path_to(mortal, minotaur.global_transform.origin)
			return true
		
		elif mortal.path_ind >= mortal.path.size() or mortal.path.size() <= 2:
			if game_starting:
				go_to_corner(mortal, index)
			else:
				random_path(mortal)
			return true
		
	return false


func path_near_minotaur(mortal: Mortal):
	var x = 0
	var y = 0
	
	if mortal.global_transform.origin.x < minotaur.global_transform.origin.x:
		x = -7
	else:
		x = 7
	if mortal.global_transform.origin.z < minotaur.global_transform.origin.z:
		y = -7
	else:
		y = 7
		
	path_to(mortal, minotaur.global_transform.origin + Vector3(x, 0, y))


func go_to_corner(mortal: Mortal, index: int):
	var vector

	# Assign each mortal to a different quad.
	if index < 2:
		if index % 2 == 0:
			vector = Vector3(rand_range(-maze_size.x / 2, 0), 1, rand_range(-maze_size.y / 2, 0))
		else:
			vector = Vector3(rand_range(-maze_size.x / 2, 0), 1, rand_range(0, maze_size.y / 2))
	else:
		if index % 2 == 0:
			vector = Vector3(rand_range(0, maze_size.x / 2), 1, rand_range(-maze_size.y / 2, 0))
		else:
			vector = Vector3(rand_range(0, maze_size.x / 2), 1, rand_range(0, maze_size.y / 2))
		
	path_to(mortal, vector)


func random_path(mortal: Mortal):
	var random_pos = random_valid_vector3()
	while mortal.global_transform.origin.distance_to(random_pos) < 4:
		random_pos = random_valid_vector3()
	mortal.set_path(random_pos)


func path_to(mortal: Mortal, position: Vector3):
	mortal.set_path(position)


func spartan_defeat():
	pass


func minotaur_health():
	minotaur_seen = true
	game_starting = false
	return 100


func find_node_by_name(root, name):

	if(root.get_name() == name): return root

	for child in root.get_children():
		if(child.get_name() == name):
			return child

		var found = find_node_by_name(child, name)

		if(found): return found

	return null
