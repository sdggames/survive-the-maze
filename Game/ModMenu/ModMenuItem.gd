class_name ModMenuItem
extends VBoxContainer

export var font: DynamicFont


func _init(mod_name: String, menu_font: DynamicFont):
	font = menu_font
	generate_menu(mod_name)


func generate_menu(mod_name: String):
	name = mod_name
	
	# Add a title
	_new_label("\n" + name.capitalize())
	
	match ModLoader.get_mod_type(name):
		Mod.ModType.AUTO_LOAD:
			var box = _new_checkbox("Enable " + name.capitalize())
			box.connect("toggled", self, "_on_Enable_toggled" )
			
			if ModLoader.get_variant_count(name) > 1:
				_new_label("Variant to load:")
				var drop = _new_dropdown()
				_add_variants(drop)
				drop.connect("item_selected", self, "_on_VariantSelect_item_selected")
			
		Mod.ModType.SELECT_ONE:
			var box = _new_checkbox("Enable " + name.capitalize())
			box.connect("toggled", self, "_on_Enable_toggled" )

			_new_label("Variant to load:")
			var drop = _new_dropdown()
			_add_variants(drop)
			drop.connect("item_selected", self, "_on_VariantSelect_item_selected")
			
		Mod.ModType.SELECT_ANY:
			_new_label("Enable any of the following variants:")
			var index = 0
			for variant in ModLoader.get_variant_names(name):
				var box = _new_checkbox(variant.replace(".pck", "").capitalize())
				box.connect("toggled", self, "_on_VariantToggle_toggled", [index])
				index += 1


func _new_label(text: String):
	var label = Label.new()
	add_child(label)
	label.text = text
	label.add_font_override("font", font)


func _new_checkbox(text: String) -> CheckBox:
	var checkbox = CheckBox.new()
	add_child(checkbox)
	checkbox.text = text
	checkbox.pressed = true
	checkbox.add_font_override("font", font)
	return checkbox


func _new_dropdown() -> OptionButton:
	var drop = OptionButton.new()
	add_child(drop)
	drop.add_font_override("font", font)
	return drop


func _add_variants(drop_down: OptionButton):
	for variant in ModLoader.get_variant_names(name):
		drop_down.add_item(variant.replace(".pck", "").capitalize())


func _on_Enable_toggled(button_pressed: bool):
	ModLoader.set_mod_active(name, button_pressed)


func _on_VariantSelect_item_selected(index: int):
	ModLoader.select_active_mod_variant(name, index)


func _on_VariantToggle_toggled(button_pressed: bool, index: int):
	ModLoader.set_mod_variant_state(name, index, button_pressed)
