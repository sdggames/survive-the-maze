class_name ModMenu
extends ScrollContainer

export var menu_font: DynamicFont

onready var mod_list = $ModList


func _ready():
	for mod in ModLoader.get_mod_names():
		var mod_menu = ModMenuItem.new(mod, menu_font)
		mod_list.add_child(mod_menu)


func _on_ReloadButton_pressed():
	ModLoader.reload_mods()
