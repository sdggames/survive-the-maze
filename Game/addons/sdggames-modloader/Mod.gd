class_name Mod
extends Node
# Used by the ModLoader to track one or more related resource packs.

# Specifies if this folder contains an autoload mod, radio-button style
# selectable mods, or multiple mod variants that can co-exist
enum ModType {
	NONE,
	AUTO_LOAD,
	SELECT_ONE,
	SELECT_ANY,
}

# Holds all of the data needed for a single pack file to be loaded and used.
class ModVariant:
	var name: String
	var path: String
	var enabled: bool
	var instance

# What kind of mod is this?
export(ModType) var mod_type = ModType.NONE
# A list of all pack files.
export var mod_variants: Array
# The full path to this directory (not res://)
export var resource_path: String
# Is the overall mod active (overrides variant toggle states)
export var is_active: bool = true

# Keep a reference to the singleton so we can unload it later.
var mod_singleton


# Fill out the necessary info to track and load a mod.
func add_mod(name : String, path: String):
	var mod = ModVariant.new()
	mod.name = name
	mod.path = path
	mod.enabled = false
	
	mod_variants.append(mod)


# Turn mods on or off for the first time based on the mod type.
func initialize_mod():
	if mod_type == ModType.NONE:
		push_error("The mod folder " + resource_path + " does not contain a ModType file!")
	if mod_variants.size() == 0:
		push_error("The mod folder " + resource_path + " did not contain any package files!")
	
	if mod_type == ModType.SELECT_ANY:
		for mod in mod_variants:
			mod.enabled = true
	else:
		mod_variants[0].enabled = true


# Load any enabled mod variants into the game's active resource pool.
func load_mod():
	for mod in mod_variants:
		if is_active and mod.enabled:
			if ProjectSettings.load_resource_pack(mod.path):
				Print.info("Mod " + mod.name + " at location " + mod.path + " is enabled")
			else:
				Print.error("Mod error, no resource at " + mod.path + " exists!")
				assert(false)


# Enables one mod variant and disables all others. ONLY WORKS FOR AUTOLOAD AND SELECT_ONE MODES!
# Does not load any files, call load_mod to commit changes.
func select_active_mod_variant(index: int):
	assert(mod_type == ModType.AUTO_LOAD or mod_type == ModType.SELECT_ONE)
	assert(index < mod_variants.size() and index >= 0)
	disable_mod_variants()
	mod_variants[index].enabled = true


# Enables or disables the mod variant at index, ONLY WORKS FOR SELECT_ANY MODE!
# Does not load any files, call load_mod to commit changes.
func set_mod_variant_state(index: int, enabled: bool):
	assert(mod_type == ModType.SELECT_ANY)
	assert(index < mod_variants.size() and index >= 0)
	mod_variants[index].enabled = enabled


# Unloads any previously used singleton, selects and loads a new mod variant, then
# loads and returns the singleton for this variant. ONLY WORKS FOR AUTOLOAD MODE!
func change_active_singleton_variant(index: int):
	free_mod_singleton()
	select_active_mod_variant(index)
	load_mod()
	return activate_mod_singleton()


# Returns an array containing all of the mod variant names.
func get_variant_names():
	var names = []
	for mod in mod_variants:
		names.append(mod.name)
	return names


# Activates the singleton that was loaded, ONLY WORKS FOR AUTOLOAD MODE!
func activate_mod_singleton():
	assert(mod_type == ModType.AUTO_LOAD)
	free_mod_singleton()
	if is_active:
		
		# Yes, I know that there is _get_modified_time error here for png and jpg files.
		# I did a lot of research, and this is an engine-level issue that will be fixed
		# in future version of Godot. https://github.com/godotengine/godot/issues/35179
		# Reddit suggested releasing a custom version of the editor with this tool, but it
		# doesn't actually break anyting (besides just cluttering the error output), so I'm
		# just leaving a note here and moving on. There should be a fix in the near-ish future.
		# https://www.reddit.com/r/godot/comments/n5pbwt/whats_the_best_way_to_handle_an_engine_error_in/gx374ik/?context=3
		var scene = ResourceLoader.load(name + ".tscn")
		if scene == null:
			push_error("Singleton named \"res://" + name + ".tscn\" does not exist!")
		else:
			mod_singleton = scene.instance()
			get_node("/root").call_deferred("add_child", mod_singleton)
			Print.debug("Loaded the singleton " + name + ".tscn at root node")
	return mod_singleton


# Frees the singleton that was loaded, ONLY WORKS FOR AUTOLOAD MODE!
func free_mod_singleton():
	assert(mod_type == ModType.AUTO_LOAD)
	if mod_singleton != null:
		mod_singleton.queue_free()
		mod_singleton = null


# Sets all mod variants to "disabled"
func disable_mod_variants():
	for mod in mod_variants:
		mod.enabled = false
