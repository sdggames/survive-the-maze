class_name MazeNode
extends Spatial

export var is_lit: bool = false
export var is_wall: bool = false
var wall_object: Spatial = null
var light_probes: Array = []
var areas_overlapping = 0

func _ready():
	_set_lit(false)


# Check if this node is currently visible by polling the nearby light probes.
func _process(_delta):
	if areas_overlapping > 0:
		var lit = false
		
		for probe in light_probes:
			if probe.is_lit:
				lit = true
				break
		
		if is_lit != lit:
			_set_lit(lit)


func _set_lit(lit: bool):
	is_lit = lit
	$ShadowMask.visible = is_lit
	if is_wall:
		wall_object.apply_shadows(is_lit)


func _on_area_entered(_area):
	areas_overlapping += 1


func _on_area_exited(_area):
	areas_overlapping -= 1
