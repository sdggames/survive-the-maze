extends Spatial

export var probe_ingnore_distance = 1.75
export var lit_material: Material
export var shadow_material: Material

onready var position = $Area.global_transform.origin
var is_lit := false
var lights_in_range := []
var probe_group := 0


func _ready():
	$MeshInstance.visible = GlobalSettings.debug_draw_light_probes
	probe_group = GlobalSettings.get_next_probe_group()


func _process(_delta):
	if lights_in_range.size() > 0 and GlobalSettings.current_probe_group == probe_group:
		_calculate_visibility()


func _set_is_lit(lit: bool):
	if is_lit != lit:
		is_lit = lit
		if GlobalSettings.debug_draw_light_probes:
			if lit:
				$MeshInstance.material_override = lit_material
			else:
				$MeshInstance.material_override = shadow_material


func _on_area_entered(light):
	lights_in_range.append(light)


func _on_area_exited(light):
	lights_in_range.erase(light)
	if lights_in_range.size() == 0:
		_set_is_lit(false)


# Cast a ray to one light, see if it hits any other walls
func _calculate_visibility():
	var lit = false

	for light in lights_in_range:
		if _cast_ray_to_single_light(light.global_transform.origin):
			lit = true
			break

	_set_is_lit(lit)


func _cast_ray_to_single_light(light_pos) -> bool:
	var ray_end = _get_nearest(position, light_pos)
	
	if position.distance_to(light_pos) < 1.5:
		return true
	
	var ray = get_world().direct_space_state.intersect_ray(light_pos, ray_end, [], 4)
	if ray.empty():
		return true
	return false


func _get_nearest(self_pos: Vector3, light_pos: Vector3) -> Vector3:
	var direction = (light_pos - self_pos).normalized()
	return self_pos + (direction * probe_ingnore_distance)

