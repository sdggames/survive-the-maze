class_name ProcMazeGen
extends Node

const placement_threshold = 10


static func generate_maze(maze_string: PoolStringArray, maze_size: Vector2) -> PoolStringArray:	
	for i in range(0, maze_size.x - 1):
		for j in range (0, maze_size.y - 1):
			# Populate some of the tiles in a vaguely maze-like fashion, using
			# an extremely simple algorithm.
			if (i % 4 == 0 and j % 4 == 0):
				var a = (randi() & 1) * 2
				var b = 2 - a
				
				# Create the base square, make it double-thick 
				if rand_range(0, 100) > placement_threshold:
					if maze_string[i][j] == "?":
						maze_string[i][j] = "X"
					if maze_string[i+1][j] == "?":
						maze_string[i+1][j] = "X"
					if maze_string[i][j+1] == "?":
						maze_string[i][j+1] = "X"
					if maze_string[i+1][j+1] == "?":
						maze_string[i+1][j+1] = "X"
				
				# Create a square to the side or bottom, make it double-thick.
				if rand_range(0, 100) > placement_threshold and i + 3 < maze_size.x and j + 3 < maze_size.y:
					if maze_string[i+a][j+b] == "?":
						maze_string[i+a][j+b] = "X"
					if maze_string[i+a+1][j+b] == "?":
						maze_string[i+a+1][j+b] = "X"
					if maze_string[i+a][j+b+1] == "?":
						maze_string[i+a][j+b+1] = "X"
					if maze_string[i+a+1][j+b+1] == "?":
						maze_string[i+a+1][j+b+1] = "X"
			
			# If we didn't turn this into a wall, make it a floor tile
			if maze_string[i][j] == "?":
				maze_string[i][j] = " "
	
	return maze_string
