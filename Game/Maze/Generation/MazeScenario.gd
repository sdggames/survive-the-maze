class_name MazeScenario
extends Resource

export(String) var scenario_name
export(String) var maze_description
export(int) var number_of_teams
export(String, MULTILINE) var maze
export(Dictionary) var mortal_definitions: Dictionary
export(Dictionary) var item_definitions: Dictionary

var maze_array: PoolStringArray
var maze_size: Vector2


func generate_new_maze_array():
	maze_array = maze.split("\n", false, 0)
	maze_size = Vector2(maze_array.size(), maze_array[0].length())
