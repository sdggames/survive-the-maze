class_name MortalDefinition 
extends Resource

# Helper class stores a mortal definition. Used by MazeScenario
export(int) var team_id
export(String) var mortal_type
