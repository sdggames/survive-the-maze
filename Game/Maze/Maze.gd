class_name Maze
extends Spatial

export(Resource) var maze_scenario
export(Resource) var team_base
export(Resource) var maze_node
export(Resource) var light_probe_node
export(Dictionary) var maze_objects
export var wall_refresh_time := 0.2
export var floor_refresh_time := 0.5

var maze_size: Vector2
var maze: Array = []
var teams = {}
var light_probes: Dictionary = {}


func _ready():
	# Convert the string into a string array that can be more easily indexed.
	maze_scenario.generate_new_maze_array()
	# Transposed to match the file orientation.
	maze_size = Vector2(maze_scenario.maze_size.y, maze_scenario.maze_size.x)
	
	generate_maze()


func generate_maze():
	# Fill out the procedural content. Now we have a string array representing the maze.
	var maze_array = ProcMazeGen.generate_maze(maze_scenario.maze_array, maze_scenario.maze_size)
	
	# Scale the floor.
	$Floor.scale = Vector3(maze_size.x, 1, maze_size.y)
	
	# Create the physical maze to match the string array.
	maze.resize(int(maze_size.x))
	for	x in maze_size.x:
		maze[x] = []
		maze[x].resize(maze_size.y)
		for y in maze_size.y:
			var worldPos = Vector2(x - (maze_size.x / 2),
					y - (maze_size.y / 2))
			
			#  Transposed to fix the maze file orientation.
			var c = maze_array[y][x]
			
			maze[x][y] = _generate_maze_node(worldPos, c)
	
	# Set up the AI and pathfinding grid for each team.
	for team in $Teams.get_children():
		team.init(maze)


func _generate_maze_node(pos: Vector2, id: String) -> Spatial:
	var node: MazeNode = maze_node.instance()
	node.transform.origin = Vector3(pos.x, 0, pos.y)
	
	# Connect the light probes for this node.
	node.light_probes.append(_get_light_probe(pos + Vector2(-0.5, -0.5)))
	node.light_probes.append(_get_light_probe(pos + Vector2(0.5, -0.5)))
	node.light_probes.append(_get_light_probe(pos + Vector2(-0.5, 0.5)))
	node.light_probes.append(_get_light_probe(pos + Vector2(0.5, 0.5)))
	
	# Give it a name and add it to the tree.
	node.name = str(pos.x) + "_" + str(pos.y)
	$Nodes.add_child(node)
	
	# We are using this as a char
	assert(id.length() == 1)

	if id == "X":
		var wall = maze_objects["Wall"].instance()
		node.add_child(wall)
		node.wall_object = wall
		node.is_wall = true
	
	elif id == "*":
		var wall = maze_objects["SolidWall"].instance()
		node.add_child(wall)
		node.wall_object = wall
		node.is_wall = true
	
	elif id != " ":
		if maze_scenario.mortal_definitions.has(id):
			# The node doesn't know about this one since mortals move so much.
			_place_mortal(pos, id)
		elif maze_scenario.item_definitions.has(id):
			var _x = _place_item(pos, id)
		else:
			Print.error("The object " + id + " does not exist! Check the definitions!")
	
	return node


func _place_mortal(pos: Vector2, key: String):
	var mortal_def: MortalDefinition = maze_scenario.mortal_definitions[key]
	var mortal = maze_objects[mortal_def.mortal_type].instance()
	mortal.transform.origin = Vector3(pos.x, mortal.transform.origin.y, pos.y)
	mortal.team = maze_scenario.mortal_definitions[key].team_id
	var team = _get_team(mortal_def.team_id)
	if mortal.team == GlobalSettings.player_team_id:
		team.is_player = true
		mortal.selected = true
	team.add_child(mortal)


func _get_team(key) -> Spatial:
	if teams.has(key):
		return teams[key]
	else:
		var team: Spatial = team_base.instance()
		team.name = "Team_" + str(key)
		$Teams.add_child(team)
		teams[key] = team
		return team


func _place_item(pos: Vector2, key: String) -> Spatial:
	var item_def: ItemDefinition = maze_scenario.item_definitions[key]
	var item = maze_objects[item_def.item_name].instance()
	item.transform.origin = Vector3(pos.x, item.transform.origin.y, pos.y)
	$Items.add_child(item)
	return item


func _get_light_probe(position: Vector2):
	var probe = null
	if light_probes.has(position):
		probe = light_probes[position]
	else:
		probe = light_probe_node.instance()
		probe.transform.origin = Vector3(position.x, 0.25, position.y)
		
		probe.name = str(position.x) + "_" + str(position.y)
		$LightProbes.add_child(probe)
		light_probes[position] = probe
		
	return probe
