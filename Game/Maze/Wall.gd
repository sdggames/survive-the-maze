extends MeshInstance
export var isSolid: bool = false


func _ready():
	if !isSolid:
		get_surface_material(0).set_shader_param("mask_texture1", get_node("/root/Main/Mask1").get_texture())
		get_surface_material(0).set_shader_param("mask_texture2", get_node("/root/Main/Mask2").get_texture())


func apply_shadows(is_lit):
	if is_lit:
		$StaticBody.set_collision_layer_bit(0, true)
	else:
		$StaticBody.set_collision_layer_bit(0, false)
