shader_type spatial;

// Simplified Godot spatial shader uniforms and settings:
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;

// The texture to use as a color mask.
// For these examples, it will be the ColorMask viewport texture.
uniform sampler2D mask_texture1 : hint_black;
uniform sampler2D mask_texture2 : hint_black;


// Godot generated code:
// It scales and offsets the UV vector by the inputted uniforms.
void vertex() {
	UV=UV*uv1_scale.xy+uv1_offset.xy;
}


void fragment() {
	// Simplified Godot spatial shader code:
	vec2 base_uv = UV;
	ALBEDO = albedo.rgb;
	METALLIC = metallic;
	ROUGHNESS = roughness;
	SPECULAR = specular;
	
	// Everything below this line is the color masking code!
	//
	// First, get the pixel from the mask texture at the screen UV position. This will give the pixel at this fragment position
	// relative to the screen, and since the ColorMask viewport is synced with the main screen/viewport, we can use this to get
	// the pixel at the correct position in the ColorMask viewport.
	vec4 mask_pixel1 = texture(mask_texture1, SCREEN_UV);
	vec4 mask_pixel2 = texture(mask_texture2, SCREEN_UV);
	
	// Because we know the ColorMask is going to be black/white, we can simply check if the pixel returned is over 0.9.
	// If it is, then change the output ALBEDO color to red.
	if (mask_pixel1.r <= 0.9 || mask_pixel2.r <= 0.9)
	{
		discard;
	}
}
