class_name PathEngine
extends Spatial

export var can_traverse_unlit: bool = false

# Used to show paths
export(PackedScene) var player_dot
export(PackedScene) var ai_dot
var dot_paths := {}

onready var astar_node = AStar.new()
var maze_size = Vector2(50, 50)

const B_L = Vector2(-1, -1)
const B = Vector2(0, -1)
const B_R = Vector2(1, -1)
const L = Vector2(-1, 0)
const R = Vector2(1, 0)
const T_L = Vector2(-1, 1)
const T = Vector2(0, 1)
const T_R = Vector2(1, 1)
const NON_DIAGONALS = [T, B, L, R]


func create_traversable_grid(maze: Array):
	maze_size = Vector2(maze.size(), maze[0].size())

	for y in range(maze_size.y):
		for x in range(maze_size.x):
			# Add the 3D position of this point, and allow us to find it later.
			var position = maze[x][y].transform.origin
			var index = _calculate_point_index(Vector2(x, y))
			
			astar_node.add_point(index, position)
			
			# Just set up the walls once, this won't change over time.
			if !can_traverse_unlit and maze[x][y].is_wall:
				astar_node.set_point_disabled(index, true)
	
	# Connect all of the points in the maze.
	for y in range(maze_size.y):
		for x in range(maze_size.x):
			var point = Vector2(x, y)
			if can_traverse_unlit or _is_node_enabled(point):
			
				for offset in NON_DIAGONALS:
					if can_traverse_unlit or _is_node_enabled(point + offset):
						_check_connect_points(point, point + offset)
			
			# Only move diagonally in open spaces.
			if _is_node_enabled(point + T) and _is_node_enabled(point + L):
				_check_connect_points(point, point + T_L)
			if _is_node_enabled(point + T) and _is_node_enabled(point + R):
				_check_connect_points(point, point + T_R)
			if _is_node_enabled(point + B) and _is_node_enabled(point + L):
				_check_connect_points(point, point + B_L)
			if _is_node_enabled(point + B) and _is_node_enabled(point + R):
				_check_connect_points(point, point + B_R)


func update_shadows(maze: Array):
	if can_traverse_unlit:
		for y in range(maze_size.y):
			for x in range(maze_size.x):
				if maze[x][y].is_wall:
					if maze[x][y].is_lit:
						astar_node.set_point_disabled(_calculate_point_index(Vector2(x, y)), true)
					else:
						astar_node.set_point_disabled(_calculate_point_index(Vector2(x, y)), false)


func get_point_path(start, end):
	var path_start = astar_node.get_closest_point(start, can_traverse_unlit)
	var path_end = astar_node.get_closest_point(end, can_traverse_unlit)
	var path = astar_node.get_point_path(path_start, path_end)

	if path.empty():
		# Better than nothing
		return [astar_node.get_point_position(path_start), astar_node.get_point_position(path_end)]
	else:
		return path


func mouse_to_world(mouse_position) -> Vector3:
	var space_state = get_world().direct_space_state
	var ray_origin = get_viewport().get_camera().project_ray_origin(mouse_position)
	var ray_end = ray_origin + get_viewport().get_camera().project_ray_normal(mouse_position) * 2000
	var intersection = space_state.intersect_ray(ray_origin, ray_end)

	if not intersection.empty():
		return intersection.position
	else:
		return Vector3.ZERO


func clear_debug_draw(path_id):
	# Remove the path for this particular mortal.
	if dot_paths.has(path_id):
		var dots = dot_paths[path_id]
		for dot in dots:
			dot.queue_free()
	# Drop our reference.
	dot_paths[path_id] = []


func draw_path(path: PoolVector3Array, path_id, player_is_human: bool):
	# Clear the path just in case.
	clear_debug_draw(path_id)
	# Draw the nav path.
	for index in len(path):
		var dot: MeshInstance
		if player_is_human:
			dot = player_dot.instance()
		else:
			dot = ai_dot.instance()
		dot.transform.origin = path[index]
		dot_paths[path_id].append(dot)
		add_child(dot)


func _check_connect_points(point1, point2):
	if _is_node_valid(point1) and _is_node_valid(point2):
		var index1 = _calculate_point_index(point1)
		var index2 = _calculate_point_index(point2)
		if astar_node.has_point(index1) and astar_node.has_point(index2):
			astar_node.connect_points(index1, index2)


func _is_node_valid(point: Vector2) -> bool:
	return (!_is_outside_map_bounds(point)) and astar_node.has_point(_calculate_point_index(point))


func _is_node_enabled(point: Vector2):
	return _is_node_valid(point) and not astar_node.is_point_disabled(_calculate_point_index(point))


func _is_outside_map_bounds(point):
	return point.x < 0 or point.y < 0 or point.x >= maze_size.x or point.y >= maze_size.y


func _calculate_point_index(v: Vector2):
	return v.x + maze_size.x * v.y
