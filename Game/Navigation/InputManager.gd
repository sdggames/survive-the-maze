class_name InputManager
extends Spatial

onready var cam = $Camera
onready var selection_box = $SelectionBox
var start_sel_pos := Vector2()
var box_boundaries: Rect2

signal select_mortal_under_mouse
signal select_mortals_in_box
signal move_mortals


func _enter_tree():
	GlobalSettings.input_manager = self


func connect_team(team):
	var _x = connect("select_mortal_under_mouse", team, "select_mortal")
	_x = connect("select_mortals_in_box", team, "select_mortals_in_box")
	_x = connect("move_mortals", team, "move_mortal")


func _process(_delta):
	var m_pos: Vector2 = get_viewport().get_mouse_position()
	if Input.is_action_just_pressed("r_click"):
		emit_signal("move_mortals", m_pos)
	if Input.is_action_just_pressed("l_click"):
		selection_box.start_sel_pos = m_pos
		start_sel_pos = m_pos
	if Input.is_action_pressed("l_click"):
		selection_box.m_pos = m_pos
		selection_box.is_visible = true
	else:
		selection_box.is_visible = false
	if Input.is_action_just_released("l_click"):
		_select_mortals(m_pos)


func is_mortal_in_box(mortal: Mortal) -> bool:
	return box_boundaries.has_point(cam.unproject_position(mortal.global_transform.origin))


func _select_mortals_in_box(top_left, bot_right):
	if top_left.x > bot_right.x:
		var tmp = top_left.x
		top_left.x = bot_right.x
		bot_right.x = tmp
	if top_left.y > bot_right.y:
		var tmp = top_left.y
		top_left.y = bot_right.y
		bot_right.y = tmp
	box_boundaries = Rect2(top_left, bot_right - top_left)
	emit_signal("select_mortals_in_box")


func _select_mortals(m_pos: Vector2):
	if m_pos.distance_squared_to(start_sel_pos) < 16:
		emit_signal("select_mortal_under_mouse", m_pos)
	else:
		_select_mortals_in_box(start_sel_pos, m_pos)
