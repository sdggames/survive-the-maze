extends Node

# Debug options.
export var debug_draw_navigation := false
export var debug_draw_light_probes := false

# Global Config Settings.
export var selection_radius := 1.5
export var shadow_distance := 10.0

# Global Match Settings.
export var player_team_id: int
export var game_mode: int = GAMEMODE_PLAYER_MINOTAUR

# Global resources.
export(Resource) var input_manager

# Light probe refresh timer
export var light_probe_refresh_groups := 4
var current_probe_group := 0


# Game modes
enum {
	GAMEMODE_PLAYER_MINOTAUR = 0,
	GAMEMODE_PLAYER_SPARTAN = 1,
}


func get_next_probe_group() -> int:
	current_probe_group += 1
	if current_probe_group >= light_probe_refresh_groups:
		current_probe_group = 0
	return current_probe_group


func _ready():
	Console.add_command("drawNavigation", self, '_toggle_nav_debug_draw')\
		.set_description("Visualizes the A* Pathfinding outputs for all" + \
			"mortals in the maze. Toggles on and off.")\
		.register()
	Console.add_command("drawLightProbes", self, '_toggle_light_probe_draw')\
		.set_description("Shows the light probes in the maze. Toggles on and off.")\
		.register()


func _process(_delta):
	# Update the probe group to refresh.
	var _x = get_next_probe_group()


func _toggle_nav_debug_draw():
	debug_draw_navigation = !debug_draw_navigation


func _toggle_light_probe_draw():
	debug_draw_light_probes = !debug_draw_light_probes
	for probe in get_tree().get_nodes_in_group("light_probes"):
		probe.visible = debug_draw_light_probes


func _exit_tree():
	Console.remove_command("drawNavigation")
