extends Spatial

export(Resource) var maze_packed
export(Resource) var default_scenario
export(Resource) var main_menu_scenario
export(Resource) var team1_lighting_environment
export(Resource) var team2_lighting_environment
export var drain_speed = 0.1
export var auto_start = true
var maze: Maze
var health_draining = 0
var drain = 1.0
var health = 100.0
var menu = true
var remaining_spartans = 4


func _ready():
	randomize()
	drain = drain_speed * ($CameraBase/Camera/CanvasLayer/Label/HSlider.value / 100.0)
	if auto_start:
		_on_Team1_Button_pressed()
	else:
		_create_maze(main_menu_scenario, 0)


func _process(_delta):
	if health_draining > 0:
		health -= drain
		$CameraBase/Camera/CanvasLayer/Health.show()
		$CameraBase/Camera/CanvasLayer/Health.value = health
		health_draining -= 1
	else:
		health = health + drain * 2
		if health > 100:
			health = 100.0
		$CameraBase/Camera/CanvasLayer/Health.hide()


func _on_Team1_Button_pressed():
	_create_maze(default_scenario, 0)
	$CameraBase/Camera/CanvasLayer/Button.hide()
	$CameraBase/Camera/CanvasLayer/Button2.hide()
	$CameraBase/Camera/CanvasLayer/Label.hide()
	$CameraBase/Camera.environment = team1_lighting_environment
	# TODO: Make this dependent on other things!
	$CameraBase/Camera/CanvasLayer/Health/Label2.text = "Get out of the light!"
	menu = false
	remaining_spartans = 4


func _on_Team2_Button_pressed():
	_create_maze(default_scenario, 1)
	$CameraBase/Camera/CanvasLayer/Button.hide()
	$CameraBase/Camera/CanvasLayer/Button2.hide()
	$CameraBase/Camera/CanvasLayer/Label.hide()
	$CameraBase/Camera.environment = team2_lighting_environment
	# TODO: Make this dependent on other things!
	$CameraBase/Camera/CanvasLayer/Health/Label2.text = "Keep him in the light!"
	menu = false
	remaining_spartans = 4


func spartan_defeat():
	remaining_spartans -= 1
	if remaining_spartans == 0:
		_game_over(0)


func minotaur_health():
	if !menu:
		health_draining = 2
		if health < 0:
			_game_over(1)
	return health


func _game_over(winning_team: int):
	$CameraBase/Camera/CanvasLayer/Button.show()
	$CameraBase/Camera/CanvasLayer/Button2.show()
	$CameraBase/Camera/CanvasLayer/Label.show()
	if GlobalSettings.player_team_id == winning_team:
		$CameraBase/Camera/CanvasLayer/Label.text = "       You Win!"
	else:
		$CameraBase/Camera/CanvasLayer/Label.text = "      You Lose!"
	menu = true


func _create_maze(scenario, team: int):
	if maze:
		maze.queue_free()
		yield(get_tree(),"idle_frame")
	maze = maze_packed.instance()
	GlobalSettings.player_team_id = team
	maze.maze_scenario = scenario
	add_child(maze)


func _on_HSlider_value_changed(value):
	drain = drain_speed * (value / 100.0)
