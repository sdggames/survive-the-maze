class_name Mortal
extends KinematicBody

enum MortalType {
	MINOTAUR,
	SPARTAN
}

export var team: int = 0
export var selected := false
export var move_speed := 12
export var mortal_type := MortalType.MINOTAUR
export var is_repath_active := false
export var steps_before_repath := 2
export var items_if_player := []
var path = []
var path_ind = 0
var path_engine: PathEngine = null
onready var nav = get_parent()


func _ready():
	if team == GlobalSettings.player_team_id:
		for item in items_if_player:
			add_child(item.instance())


func set_path(path_end):
	path = path_engine.get_point_path(global_transform.origin, path_end)
	path_ind = 0


func repath():
	path = path_engine.get_point_path(global_transform.origin, path[-1])
	path_ind = 0


func _process(_delta):
	if is_repath_active and path_ind > steps_before_repath:
		repath()


func _physics_process(_delta):
	if path_ind < path.size():
		var move_vec = (path[path_ind] - global_transform.origin)
		if move_vec.length() < 1.2:
			path_ind += 1
		else:
			var _d = move_and_slide(move_vec.normalized() * move_speed, Vector3(0, 1, 0))
		
	# Use rays and distance to detect potential collisions.
	var mortals = get_tree().get_nodes_in_group("mortals")
	for mortal in mortals:
		if mortal.team != team:
			var other_pos = mortal.global_transform.origin
			var distance = global_transform.origin.distance_to(other_pos)
				
			if distance < 1.25:
				if mortal_type == MortalType.SPARTAN:
					# Report me as dead
					for main in get_tree().get_nodes_in_group("main"):
						main.spartan_defeat()
					queue_free()
			elif distance < GlobalSettings.shadow_distance:
				var ray = get_world().direct_space_state.intersect_ray(
						other_pos, global_transform.origin, [], 4)
				if ray.empty():
					for main in get_tree().get_nodes_in_group("main"):
						if main.minotaur_health() < 0:
							queue_free()


func select():
	selected = true
	if team == GlobalSettings.player_team_id:
		$SelectionRing.show()


func deselect():
	selected = false
	if team == GlobalSettings.player_team_id:
		$SelectionRing.hide()
